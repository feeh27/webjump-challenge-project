# Estrutura base para Ecommerce

Esse projeto permite a criação de um Ecommerce simples

## Requisitos

 - PHP 7.2 ou mais recente
 - Composer

## Instalação

É preferível que seja instalado via [composer](https://getcomposer.org/):

```bash
composer create-project feeh27/ecommerce
```

## Contribuidores desse repositório

Felipe Dominguesche - [Linkedin](https://linkedin.com/in/felipe-dominguesche)
